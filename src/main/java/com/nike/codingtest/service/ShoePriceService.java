package com.nike.codingtest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nike.codingtest.model.DiscountedShoePrice;
import com.nike.codingtest.model.ShoePrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Service
public class ShoePriceService {

    private final RestTemplate restTemplate;

    @Autowired
    public ShoePriceService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getShoePrice(String id) {
        String url = "https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id="+id;
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    public DiscountedShoePrice getDiscountedShoePrice(String id) throws JsonProcessingException {
        String shoePrice = getShoePrice(id);  // reuse the existing logic to get original shoe price
        ObjectMapper objectMapper = new ObjectMapper();
        ShoePrice shoePriceObj = objectMapper.readValue(shoePrice, ShoePrice.class); // Convert string to ShoePrice

        Integer originalShoePrice = shoePriceObj.getShoePrice();
        if (originalShoePrice != null) {
            BigDecimal decimal = BigDecimal.valueOf(0.6 * originalShoePrice);   // set a common flat discount of 40%
            return DiscountedShoePrice.builder()
                    .shoePrice(originalShoePrice)
                    .discountedShoePrice(decimal.setScale(2))   // set scale to 2
                    .build();
        }
        return null;

    }
}
