package com.nike.codingtest.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nike.codingtest.model.DiscountedShoePrice;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class ShoePriceServiceTest {

    @InjectMocks
    private ShoePriceService shoePriceService;

    @Mock
    private RestTemplate restTemplate;

    private String shoePrice;

    private String url;
    private DiscountedShoePrice discountedShoePrice;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        shoePrice = "{\"shoePrice\":166}";
        url = "https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1";
        discountedShoePrice = DiscountedShoePrice.builder()
                .shoePrice(166)
                .discountedShoePrice(new BigDecimal("99.60"))
                .build();
    }

    @Test
    void getShoePrice() {
        when(restTemplate.getForObject(url, String.class)).thenReturn(shoePrice);
        String shoePrice1 = shoePriceService.getShoePrice("1");
        Assertions.assertEquals(shoePrice1, shoePrice);
    }

    @Test
    void getDiscountedShoePrice() throws JsonProcessingException {
        when(restTemplate.getForObject(url, String.class)).thenReturn(shoePrice);
        DiscountedShoePrice discountedShoePrice1 = shoePriceService.getDiscountedShoePrice("1");
        Assertions.assertEquals(discountedShoePrice1, discountedShoePrice);
    }
}