package com.nike.codingtest.model;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Data
@SuperBuilder
public class DiscountedShoePrice extends ShoePrice {

    private BigDecimal discountedShoePrice;
}
