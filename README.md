### Nike Interview Backend Code Challenge [Java]

_The provided code document should contain more details._

This project uses Maven & Spring Boot (Web Starter Pack). So you will need to install `java (> 1.8)` & `maven` on your machine first.
For more information, you can visit the [Spring Boot docs](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started.introducing-spring-boot)

1. You can run `mvn dependency:tree` to list/install the maven dependencies used in this project.
2. To run the server (on port 8081), use this command: `mvn spring-boot:run`

APIs:

1. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```
2. Get discounted price and original price for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/discounted-shoe-price/1

Response:
{
    "shoePrice": 147,
    "discountedPrice": 99.60
}
```

#### Implementation steps:
1. Creating ShoePriceControllerTest class
2. Adding getShoePrice and getDiscountedShoePrice test cases in ShoePriceControllerTest class
3. Creating ShoePriceController class
4. Building new Api /api/discounted-shoe-price/{id}
5. Creating ShoePriceService class
6. Implementing getting discounted shoe price logic in getDiscountedShoePrice function
7. Creating ShoePriceServiceTest class
8. Adding getShoePrice and getDiscountedShoePrice test cases in ShoePriceServiceTest class
9. Pass all test cases

#### TODO
1. Exception Handling
2. Universal Response Format
3. Integration Testing
4. Integrate a DB to store shoe models, minPrice and maxPrice data
5. Implementing a getAll api. In this api, it will return all the shoe models, minPrice, maxPrice, original shoe prices and discounted shoe prices (assuming the shoe prices api could support query a list of shoe prices)
6. In the new getAll api, it will apply the different discount to the different shoe models as well
7. To integrate SonarQube scan in the .gitlab-ci.yml
8. TO add deploy stage in the .gitlab-ci.yml