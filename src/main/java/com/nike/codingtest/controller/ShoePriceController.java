package com.nike.codingtest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.nike.codingtest.model.DiscountedShoePrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.nike.codingtest.service.ShoePriceService;

@RestController
@RequestMapping(value = "/api")
public class ShoePriceController {

    @Autowired
    private ShoePriceService shoePriceService;

    @GetMapping("/shoe-price/{id}")
    @ResponseBody
    public String getShoePrice(@PathVariable String id) {
        return shoePriceService.getShoePrice(id);
    }

    @GetMapping("/discounted-shoe-price/{id}")
    @ResponseBody
    public DiscountedShoePrice getDiscountedShoePrice(@PathVariable String id) throws JsonProcessingException {
        return shoePriceService.getDiscountedShoePrice(id);
    }
}
