package com.nike.codingtest.controller;

import com.nike.codingtest.model.DiscountedShoePrice;
import com.nike.codingtest.service.ShoePriceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@WebMvcTest
class ShoePriceControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private ShoePriceService shoePriceService;


    private String shoeId;
    private String shoePrice;
    private DiscountedShoePrice discountedShoePrice;

    @BeforeEach
    public void init() {
        shoeId = "1";
        shoePrice = "{'shoePrice':166}";
        discountedShoePrice = DiscountedShoePrice.builder()
                .shoePrice(166)
                .discountedShoePrice(BigDecimal.valueOf(99.60))
                .build();
    }

    @Test
    void getShoePrice() throws Exception {
        when(shoePriceService.getShoePrice(shoeId)).thenReturn(shoePrice);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/shoe-price/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.shoePrice").value(166));
    }

    @Test
    void getDiscountedShoePrice() throws Exception {
        when(shoePriceService.getDiscountedShoePrice(shoeId)).thenReturn(discountedShoePrice);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/discounted-shoe-price/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.discountedPrice").value(99.60));
    }
}